use crate::prelude::*;

property!(
    /// `FontIcon` describes the font icon of a widget.
    FontIcon(String) : &str
);
