use crate::prelude::*;

property!(
    /// `Count` simple count property.
    Count(usize)
);
