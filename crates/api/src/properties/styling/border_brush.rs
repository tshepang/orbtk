use crate::{prelude::*, utils::*};

property!(
    /// `BorderBrush` describes the border brush.
    BorderBrush(Brush) : &str,
    String
);
